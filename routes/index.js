const express = require('express');

const router = express.Router();
const usersRoutes = require('./users.routes');
const cardsRoutes = require('./cards.routes');
const badReqRoutes = require('./404.routes');

router.use('/users', usersRoutes);
router.use('/cards', cardsRoutes);
router.use(/.+/, badReqRoutes);

module.exports = router;
