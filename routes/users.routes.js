const express = require('express');
const services = require('../services');

const router = express.Router();
const dataFile = 'users.json';

router.get('/', (req, res) => {
  services.readFile(dataFile)
    .then((data) => {
      res.send(data);
    });
});

router.get('/:id', (req, res) => {
  services.readFile(dataFile)
    .then((data) => {
      const result = JSON.parse(data);

      if (Array.isArray(result)) {
        const user = result.find(({ _id: id }) => id === req.params.id);

        if (user === undefined) throw Error;

        res.send(user);
      } else {
        res.send(result);
      }
    })
    .catch(() => {
      const error = JSON.stringify(
        { message: 'Нет пользователя с таким id' },
      );

      res.status(404).send(error);
    });
});

module.exports = router;
