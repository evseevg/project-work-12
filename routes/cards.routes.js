const express = require('express');
const services = require('../services');

const router = express.Router();
const dataFile = 'cards.json';

router.get('/', (req, res) => {
  services.readFile(dataFile)
    .then((data) => {
      res.send(data);
    });
});

module.exports = router;
