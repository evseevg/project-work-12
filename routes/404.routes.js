const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
  const error = JSON.stringify(
    { message: 'Запрашиваемый ресурс не найден' },
  );

  res.status(404).send(error);
});

module.exports = router;
