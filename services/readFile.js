const { readFile } = require('fs').promises;
const path = require('path');

module.exports.readFile = (fileName) => {
  const basePath = path.dirname(process.mainModule.filename);
  const filePath = path.join(basePath, 'data', fileName);

  return readFile(filePath, { encoding: 'utf-8' })
    .then((result) => result)
    .catch(() => {
      const err = JSON.stringify(
        { message: 'Ошибка при получении данных' },
      );

      return err;
    });
};
